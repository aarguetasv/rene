﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="WebApplication2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

  <section id="hero">
    <div class="hero-container">
      <h1>Aplicación ASP para Servidores</h1>
      <h2>Prueba de conexión a servidor SQL</h2>

        <br />
        <asp:Button ID="btn_test" class="btn-get-started" runat="server" Text="Conectar al servidor" />
        <br />
        <br />
       <h3> <asp:Label ID="lbl_status" runat="server" Text="Esperando prueba de conexión"></asp:Label></h3>

    </div>
  </section><!-- #hero -->

</asp:Content>
